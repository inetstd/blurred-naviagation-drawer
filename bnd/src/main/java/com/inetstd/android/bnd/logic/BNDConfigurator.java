package com.inetstd.android.bnd.logic;

import android.graphics.Color;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

import com.inetstd.android.bnd.R;
import com.inetstd.android.bnd.async.BlurredTask;
import com.inetstd.android.bnd.views.ElasticImageView;

/**
 * Created by vicm on 1/11/14.
 */
public class BNDConfigurator implements DrawerLayout.DrawerListener {
    private static String LOG = BNDConfigurator.class.getSimpleName();
    View mainContent;
    ElasticImageView navigationDrawer;
    DrawerLayout drawerLayout;



    int blurRadiud = 50;

    public void cfg(DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
        this.mainContent = drawerLayout.getChildAt(0);
        this.navigationDrawer = (ElasticImageView) drawerLayout.getChildAt(1);
        drawerLayout.setScrimColor(Color.TRANSPARENT);
        this.drawerLayout.setDrawerListener(this);
        ViewTreeObserver vto = mainContent.getViewTreeObserver();

        if (vto != null) {
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    new BlurredTask(blurRadiud).execute(mainContent, navigationDrawer);
                }
            });
        }
    }

    public BNDConfigurator setBlurRadiud(int blurRadiud) {
        if (blurRadiud > 0 && blurRadiud < 100) this.blurRadiud = blurRadiud;
        return this;
    }


    @Override
    public void onDrawerSlide(View view, float v) {

        navigationDrawer.updateScroll((int) (v * view.getWidth()));

    }

    @Override
    public void onDrawerOpened(View view) {
//        fakeImage.animate().alpha(0.0f).start();
    }

    @Override
    public void onDrawerClosed(View view) {
//        fakeImage.animate().alpha(0.0f).start();
    }

    @Override
    public void onDrawerStateChanged(int i) {

    }
}

