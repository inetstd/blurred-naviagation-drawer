package com.inetstd.android.bnd.views;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.inetstd.android.bnd.logic.BNDConfigurator;

/**
 * Created by vicm on 1/11/14.
 */
public class BlurredNavigationDrawerView extends FrameLayout {

    DrawerLayout drawerLayout;
    ElasticImageView elasticImageView;

    public BlurredNavigationDrawerView(Context context) {
        super(context);
    }

    public BlurredNavigationDrawerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BlurredNavigationDrawerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (getChildCount() == 1 && getChildAt(0) instanceof DrawerLayout) {
            drawerLayout = (DrawerLayout) getChildAt(0);


            new BNDConfigurator().setBlurRadiud(30).cfg(drawerLayout);
            Log.i("ok", "BNDConfigurator");
        } else {
            throw new IllegalArgumentException("Layout should have only one child and it should be android.support.v4.widget.DrawerLayout");
        }
    }


}
