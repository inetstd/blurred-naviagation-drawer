package com.inetstd.android.bnd.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.inetstd.android.bnd.R;

/**
 * Created by vicm on 1/18/14.
 */
public class TestSurface extends SurfaceView implements SurfaceHolder.Callback {

    private DrawThread drawThread;
    private SurfaceHolder holder;

    public TestSurface(Context context, AttributeSet attrs) {
        super(context, attrs);

        holder = getHolder();
        drawThread = new DrawThread(holder, getContext());
        holder.addCallback(this);

    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        drawThread.setRunning(true);
        drawThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean retry = true;
        drawThread.setRunning(false);
        while (retry)
        {
            try
            {
                // ожидание завершение потока
                drawThread.join();
                retry = false;
            }
            catch (InterruptedException e) { }
        }
    }

    class DrawThread extends Thread {
        private static final int FIELD_WIDTH = 300;
        private static final int FIELD_HEIGHT = 250;

        /** Область, на которой будем рисовать */
        private SurfaceHolder mSurfaceHolder;

        /** Состояние потока (выполняется или нет. Нужно, чтобы было удобнее прибивать поток, когда потребуется) */
        private boolean mRunning;

        /** Стили рисования */
        private Paint mPaint;

        /** Прямоугольник игрового поля */
        private Rect mField;

        /**
         * Конструктор
         * @param surfaceHolder Область рисования
         * @param context Контекст приложения
         */
        public DrawThread(SurfaceHolder surfaceHolder, Context context)
        {
            mSurfaceHolder = surfaceHolder;
            mRunning = false;

            mPaint = new Paint();
            mPaint.setColor(Color.BLUE);
            mPaint.setStrokeWidth(2);
            mPaint.setStyle(Paint.Style.STROKE);

            int left = 10;
            int top = 50;
            mField = new Rect(left, top, left + FIELD_WIDTH, top + FIELD_HEIGHT);
        }

        /**
         * Задание состояния потока
         * @param running
         */
        public void setRunning(boolean running)
        {
            mRunning = running;
        }

        @Override
        /** Действия, выполняемые в потоке */
        public void run()
        {
            while (mRunning)
            {
                Canvas canvas = null;
                try
                {
                    // подготовка Canvas-а
                    canvas = mSurfaceHolder.lockCanvas();
                    synchronized (mSurfaceHolder)
                    {
                        // собственно рисование
                        canvas.drawRect(mField, mPaint);
                    }
                }
                catch (Exception e) { }
                finally
                {
                    if (canvas != null)
                    {
                        mSurfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }
}
