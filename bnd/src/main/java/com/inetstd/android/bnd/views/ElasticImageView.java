package com.inetstd.android.bnd.views;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;

/**
 * Created by vicm on 1/10/14.
 */
public class ElasticImageView extends FrameLayout {

    TestSurface surface;

    public ElasticImageView(Context context) {
        super(context);
    }

    public ElasticImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ElasticImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setWillNotDraw(false);
    }

    int mmx = 0;
    Bitmap originalImage, adaptedImage;
    private Paint paint = new Paint();



    @Override
    protected void onDraw(Canvas canvas) {
        try {
          canvas.drawBitmap(originalImage, this.getWidth() - mmx , 0, paint);
        } catch (Exception e) {

        }

    }


    public void setOriginalImage(Bitmap bmp) {

        originalImage = bmp;
        postInvalidate();
    }

    public void updateScroll(int x) {
        this.mmx = x;

//        invalidate();
        if (mmx > 0 && originalImage != null) {

             postInvalidate();
        }
    }
}
